export interface Student {
    id: Number,
    studentNum: Number,
    name: String,
    email: String,
    grade: String
}