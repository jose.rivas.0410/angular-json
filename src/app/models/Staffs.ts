export interface Staff {
    id: Number;
    name: String;
    email: String;
    dept: String;
    phoneNum: Number;
}