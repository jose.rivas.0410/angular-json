import { Component, OnInit } from '@angular/core';
import { Staff } from '../../models/Staffs';
import { Student } from '../../models/Students';
import StaffsData from '../../staffs.json';
import StudentsData from '../../students.json';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  staffs: Staff[] = StaffsData;

  students: Student[] = StudentsData

  constructor() { }

  ngOnInit(): void {
  }

}
