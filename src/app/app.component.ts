import { Component } from '@angular/core';
import StaffsData from './staffs.json';
import { Staff } from '../app/models/Staffs'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  staffs: Staff[] = StaffsData;

  title = 'angular-json';
}
